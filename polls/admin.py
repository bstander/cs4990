from django.contrib import admin
from polls.models import Poll, Choice

class PollAdmin(admin.ModelAdmin):
	fields = ['question', 'author', 'pub_date'] # Change object page
	list_display = ('question', 'author', 'pub_date',) # List of objects

class ChoiceAdmin(admin.ModelAdmin):
	list_display = ('answer', 'poll', 'count')

admin.site.register(Poll, PollAdmin)
admin.site.register(Choice, ChoiceAdmin)

